function createNewUser() {
    const newUser = {
      firstName: '',
      lastName: '',
      getLogin: function () {
        return `${this.firstName.charAt(0).toLowerCase()}${this.lastName.toLowerCase()}`;
      },
      setFirstName: function (newFirstName) {
        if (typeof newFirstName === 'string') {
          this.firstName = newFirstName;
        } else {
          console.error('Invalid input. First name should be a string.');
        }
      },
      setLastName: function (newLastName) {
        if (typeof newLastName === 'string') {
          this.lastName = newLastName;
        } else {
          console.error('Invalid input. Last name should be a string.');
        }
      },
    };
  
    newUser.firstName = prompt('Введіть ім\'я:');
    newUser.lastName = prompt('Введіть прізвище:');
  
    return newUser;
  }
  
  const user = createNewUser();
  console.log(user.getLogin());
  